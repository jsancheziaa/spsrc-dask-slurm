# spsrc-dask-slurm

**Deployment DASK with Slurm Cluster**

Dask is a flexible library for parallel computing in Python.

Dask is composed of two parts:

- Dynamic task scheduling optimized for computation. This is similar to Airflow, Luigi, Celery, or Make, but optimized for interactive computational workloads.
- “Big Data” collections like parallel arrays, dataframes, and lists that extend common interfaces like NumPy, Pandas, or Python iterators to larger-than-memory or distributed environments. These parallel collections run on top of dynamic task schedulers.


Dask emphasizes the following virtues:

- Familiar: Provides parallelized NumPy array and Pandas DataFrame objects

- Flexible: Provides a task scheduling interface for more custom workloads and integration with other projects.

- Native: Enables distributed computing in pure Python with access to the PyData stack.

- Fast: Operates with low overhead, low latency, and minimal serialization necessary for fast numerical algorithms

- Scales up: Runs resiliently on clusters with 1000s of cores

- Scales down: Trivial to set up and run on a laptop in a single process

- Responsive: Designed with interactive computing in mind, it provides rapid feedback and diagnostics to aid humans




## Getting started


## Ansible deployment of DASK with pip
### Use Ansible
You can install a released version of Ansible with pip or a package manager. See our installation guide for details on installing Ansible on a variety of platforms.

Power users and developers can run the devel branch, which has the latest features and fixes, directly. Although it is reasonably stable, you are more likely to encounter breaking changes when running the devel branch. We recommend getting involved in the Ansible community if you want to run the devel branch.

### Requeriments
Ansible version: latest.

Example: Ubuntu version
```
sudo apt update
sudo apt upgrade
sudo apt install software-properties-common
```
Next add ppa:ansible/ansible to your system’s Software Source:
```
sudo apt-add-repository ppa:ansible/ansible
```
You need install ansible-galaxy collection
```
ansible-galaxy collection install community.crypto

```


### Configuring Inventorie File
You need to add the target hosts 
```
vi hosts
```

### Run Playbook

This configuration is for using DASK on a slurm cluster with CentOS 7 installed.

To run the playbook:
```
ansible-playbook -i hosts dask-slurm-ansible-pip.yml
```


